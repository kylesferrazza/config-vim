{
  description = "kyle's vim setup";
  inputs.flake-utils.url = "github:numtide/flake-utils";
  outputs = { self, flake-utils, nixpkgs }: flake-utils.lib.eachDefaultSystem (system:
    let
      pkgs = import nixpkgs { inherit system; };
      mods = pkgs.callPackage ./mods {};
    in {
      defaultPackage = pkgs.callPackage ./default.nix {
        inherit mods;
      };
    }
  );
}
