{ vimUtils, fetchFromGitHub, ... }:
let
  vim-schlepp = vimUtils.buildVimPlugin {
    name = "vim-schlepp";
    src = fetchFromGitHub {
      owner = "zirrostig";
      repo = "vim-schlepp";
      rev = "dfe67ad49d534e6c442d589c558da7b3ab052f03";
      sha256 = "0wd1149k1ryfs97mffhyxm4fdhbfw4xdw23v6i5kc8j8nfy0gnil";
    };
  };
in {
  plugins = [ vim-schlepp ];
  rc = ''
    vmap <unique> <up>    <Plug>SchleppUp
    vmap <unique> <down>  <Plug>SchleppDown
    vmap <unique> <left>  <Plug>SchleppLeft
    vmap <unique> <right> <Plug>SchleppRight

    vmap <unique> <S-up>   <Plug>SchleppIndentUp
    vmap <unique> <S-down> <Plug>SchleppIndentDown
  '';
}
