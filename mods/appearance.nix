{ vimPlugins, ... }:
{
  plugins = with vimPlugins; [ awesome-vim-colorschemes ];
  rc = ''
    colorscheme solarized8
    set termguicolors
    syntax enable
    set background=dark
  '';
}
