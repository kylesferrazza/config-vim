{ pkgs, ...}:
let
  modFiles = [
    ./base.nix
    ./whitespace.nix
    ./clipboard.nix
    ./appearance.nix

    ./repeat.nix
    ./airline.nix
    ./surround.nix
    ./colorizer.nix
    ./indentguides.nix
    ./fzf.nix
    ./easymotion.nix
    ./which-key.nix
    ./commentary.nix
    ./unimpaired.nix
    ./vinegar.nix
    ./schlepp.nix
    ./suda.nix
    ./tabular.nix
    ./fugitive.nix
    ./gitgutter.nix

    ./lsp.nix
    ./nix.nix
    ./fish.nix
    ./markdown.nix
  ];
  importModFile = f: pkgs.callPackage f {};
in map importModFile modFiles
