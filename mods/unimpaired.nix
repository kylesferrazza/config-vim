{ vimPlugins, ... }:
{
  plugins = with vimPlugins; [ vim-unimpaired ];
}
