{ vimPlugins, ... }:
{
  plugins = with vimPlugins; [ vim-commentary ];
}
