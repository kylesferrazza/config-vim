{ vimPlugins, ... }:
{
  plugins = with vimPlugins; [ vim-vinegar ];
}
