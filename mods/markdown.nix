{ vimPlugins, ... }:
{
  plugins = with vimPlugins; [ vim-markdown ];
  rc = ''
    let g:vim_markdown_conceal = 0
  '';
}
