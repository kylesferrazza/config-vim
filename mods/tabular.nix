{ vimPlugins, ... }:
{
  plugins = with vimPlugins; [ tabular ];
  rc = ''
    no <Leader>t :Tabularize<CR>
  '';
}
