{ vimPlugins, ... }:
{
  plugins = with vimPlugins; [ vim-which-key ];
  rc = ''
    nnoremap <silent> <leader>      :<c-u>WhichKey '<Space>'<CR>
    nnoremap <silent> <localleader> :<c-u>WhichKey  ','<CR>
  '';
}
