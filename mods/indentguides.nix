{ vimPlugins, ... }:
{
  plugins = with vimPlugins; [ vim-indent-guides ];
  rc = ''
    let g:indent_guides_enable_on_vim_startup = 1
  '';
}
