{ vimPlugins, vimUtils, fetchFromGitHub, ... }:
let
  fugitive-gitlab = vimUtils.buildVimPlugin {
    name = "fugitive-gitlab";
    src = fetchFromGitHub {
      owner = "shumphrey";
      repo = "fugitive-gitlab.vim";
      rev = "b73a8e97de95d26280082abb7f51465a3d3b239e";
      sha256 = "sha256-qrwTbyB1+FIzVJRtRGJBpFUEyoblf9MRSnuXhXJVNcY=";
    };
  };
in {
  plugins = (with vimPlugins; [ vim-fugitive vim-rhubarb ]) ++ [ fugitive-gitlab ];
}
