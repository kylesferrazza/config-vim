{ vimPlugins, ... }:
{
  plugins = with vimPlugins; [ vim-easymotion ];
  rc = ''
    map <Leader>m <Plug>(easymotion-prefix)
  '';
}
