{ ... }:
{
  rc = ''
    command! -nargs=* T 15split | terminal <args>

    set title
    set mouse=a
    set tabstop=2
    set shiftwidth=2
    set expandtab
    set autoindent
    set smartindent
    set showmatch
    set wildmenu
    set path+=**
    set clipboard=unnamedplus
    set showcmd
    set incsearch
    set inccommand=nosplit " live search and replace
    set cursorline
    set wrap lbr
    "set nohlsearch
    set nocompatible
    set noshowmode
    set conceallevel=1

    syntax on

    " NUMBER -> RELNUMBER
    set number relativenumber
    augroup numbertoggle
      au BufEnter,FocusGained,InsertLeave * set relativenumber
      au BufLeave,FocusLost,InsertEnter * set norelativenumber
    augroup END

    silent call mkdir ($HOME.'/.config/nvim/backup', 'p')
    silent call mkdir ($HOME.'/.config/nvim/swap', 'p')
    silent call mkdir ($HOME.'/.config/nvim/undo', 'p')

    set backupdir=~/.config/nvim/backup/
    set directory=~/.config/nvim/swap/

    set undodir=~/.config/nvim/undo/
    set undofile

    " netrw
    let g:netrw_banner = 0
    let g:netrw_liststyle = 3
    let g:netrw_altv = 1
    let g:netrw_winsize = 25

    " Jump to the last position when reopening a file
    if has("autocmd")
      au BufReadPost * if line("'\"") > 0 && line("'\"") <= line("$")
            \| exe "normal! g'\"" | endif
    endif

    let g:mapleader = "\<Space>"
    let g:maplocalleader = ','

    " Simple keybindings
    ino jk <esc>
    tno jk <C-\><C-n>
    " tno <C-w><C-w> <C-\><C-n><C-w><C-w>
    " tno <C-d> <C-\><C-n>:q<CR>
    map Y y$
  '';
}
