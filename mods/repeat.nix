{ vimPlugins, ... }:
{
  plugins = with vimPlugins; [ vim-repeat ];
}
