{ vimUtils, fetchFromGitHub, ... }:
let
  suda = vimUtils.buildVimPlugin {
    name = "suda";
    src = fetchFromGitHub {
      owner = "lambdalisue";
      repo = "suda.vim";
      rev = "45f88d4f0699c054af775b82c87b93b439da0a22";
      sha256 = "0apf28b569qz4vik23jl0swka37qwmbxxiybfrksy7i1yaq6d38g";
    };
  };
in {
  plugins = [ suda ];
  rc = ''
    no <Leader>W :w suda://%<CR>
  '';
}
