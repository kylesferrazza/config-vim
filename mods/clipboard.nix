{ vimPlugins, ... }:
{
  plugins = with vimPlugins; [ vim-oscyank ];
}
