{ vimPlugins, ... }:
{
  plugins = with vimPlugins; [
    fzf-vim
  ];
  rc = ''
    no <Leader>ff :GFiles<CR>
    no <Leader>sp :Rg<CR>
    no <Leader>fb :Buffers<CR>
    no <Leader>bl :BLines<CR>
    no <Leader>bc :BCommits<CR>
    no <Leader>fc :Commits<CR>
    no <Leader><Leader> :Commands<CR>
  '';
}
