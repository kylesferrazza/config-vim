{ vimPlugins, ... }:
{
  plugins = with vimPlugins; [ vim-surround ];
}
