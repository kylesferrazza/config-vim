{ vimPlugins, ... }:
{
  plugins = with vimPlugins; [ colorizer ];
  rc = ''
    no <Leader>cc :ColorToggle<CR>
  '';
}
