{ vimPlugins, ... }:
{
  rc = ''
    let g:airline_powerline_fonts = 1
    let g:airline_theme = 'solarized'
    let g:airline_solarized_bg='dark'
  '';

  plugins = with vimPlugins; [vim-airline vim-airline-themes];
}
