{ vimPlugins, ... }:
{
  plugins = with vimPlugins; [ vim-nix ];
}
